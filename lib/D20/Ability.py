class Ability(object):
    def __init__(self,score=0,saving_throw=False,actor=None):
        """
        Defines an ability that powers an actor's stats.

        :param score: The raw score of an ability.
        :param is_saving_throw: Boolean indicating if we're is_saving_throw with this skill or not.
        :param actor: The object that this ability is associated with.
        """
        if score < 0:
            score = 0
        self.is_saving_throw = False
        self.score           = score
        self.saving_throw    = saving_throw
        self.actor           = actor

    @property
    def modifier(self):
        """
        :return: The modifier for this ability.  This is the number that is often used for modifying dice rolls.
        """
        value = self.score - 10
        if value == 0:
            return 0
        else:
            return int(value/2)

    @property
    def saving_throw(self):
        """
        :return: The amount to add to a saving throw for this ability.
        """
        value = self.modifier
        if self.is_saving_throw and self.actor:
            value += self.actor.proficiency
        return value
    
    @saving_throw.setter
    def saving_throw(self,value=False):
        self.is_saving_throw = value

    def modifier_str(self):
        str = ""
        mod = self.modifier
        if mod >= 0:
            str += "+%d"%mod
        elif mod == 0:
            str += "0"
        else:
            str += "%d"%mod
        if self.is_saving_throw:
            str += "*"
        return str

    def __str__(self):
        str = "%d (%s)"%(self.score,self.modifier_str())
        return str

    def __add__(self,other):
        return Ability(self.score+other.score)

class AbilityStrength(Ability):
    pass

class AbilityDexterity(Ability):
    pass

class AbilityConstitution(Ability):
    pass

class AbilityIntelligence(Ability):
    pass

class AbilityWisdom(Ability):
    pass

class AbilityCharisma(Ability):
    pass

class Abilities(object):
    def __init__(self,str=0,dex=0,con=0,int=0,wis=0,cha=0):
        """
        Collection of ability objects that represent our actor.
        """
        self.strength       = AbilityStrength(str,actor=self)
        self.dexterity      = AbilityDexterity(dex,actor=self)
        self.constitution   = AbilityConstitution(con,actor=self)
        self.intelligence   = AbilityIntelligence(int,actor=self)
        self.wisdom         = AbilityWisdom(wis,actor=self)
        self.charisma       = AbilityCharisma(cha,actor=self)

    def __str__(self):
        value = "STR: "
        value += self.strength.__str__() + "  "
        value += "DEX: "
        value += self.dexterity.__str__() + "  "
        value += "INT: "
        value += self.intelligence.__str__() + "  "
        value += "CON: "
        value += self.constitution.__str__() + "  "
        value += "WIS: "
        value += self.wisdom.__str__() + "  "
        value += "CHA: "
        value += self.charisma.__str__() + "  "
        return value

