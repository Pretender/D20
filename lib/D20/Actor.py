from Ability import *
from Skill import *
from Item import *

class Actor(Abilities):

    def __init__(self,name="",str=0,dex=0,con=0,int=0,wis=0,cha=0):
        """
        An actor is either a player or npc in the game world that is made up of various stats.

        :param name: Name of this actor.
        :param str: STRENGTH
        :param dex: DEXTERITY
        :param con: CONSTITUTION
        :param int: INTELLIGENCE
        :param wis: WISDOM
        :param cha: CHARISMA
        """
        Abilities.__init__(self,str,dex,con,int,wis,cha)
        self.name                   = name
        self.level                  = 1
        self.skills                 = Skills(self)
        self.health                 = 0
        self.health_tmp             = 0
        self.health_max             = 0
        self.proficiency_bonus      = 2
        self.proficiencies          = list()
        self.items                  = list()
        self.conditions             = list()
        self.speed_real             = 30
        self.armor_real             = 14
        self.img_portrait           = "/Users/bgrissom/Code/D20/assets/%s.png"%self.name.lower()
        self.img_window             = "/Users/bgrissom/Code/D20/assets/bg_blue.png"

    def __str__(self):
        return self.name + " --> "+ Abilities.__str__(self)

    @property
    def armor(self):
        return self.armor_real
    @armor.setter
    def armor(self,value):
        self.armor_real = value

    @property
    def speed(self):
        return self.speed_real
    @speed.setter
    def speed(self,value):
        self.speed_real = value

    def equip_main_hand(self,item):
        """
        Equips give item in our main hand.  When we attack, we'll attack with this item.
        
        :param item: Item object to equip.
        """
        item = item

    def equip_off_hand(self,item):
        """
        Equips given item in our off hand.  If we use our bonus attack or duel-wield, then we'll attack with this item.
        
        :param item: Item object to equip.
        """
        self.off_hand = item

    def roll_attack(self,item=None,apply_modifiers=True,apply_proficiencies=True):
        """
        Rolls our attack with the item.  This is compared against an actor's armor to determine if the hit was successful or not.
        
        :param item: Item object to attack with.  if not set, then we'll attack with nothing.
        :param apply_modifiers: If set, we'll apply our ability modifiers.
        :param apply_proficiencies: If set, we'll apply our proficiency bonsues.
        :type item: ItemWeapon
        :return: Value that was rolled.

        """
        LOG.DEBUG(Helper.line_str_small())
        if not item:
            item = Item.get("unarmed")
            LOG.WARN("No weapon provided, assuming we're unarmed!")

        LOG.DEBUG("Rolling for ATK with %s"%item.name.upper())

        total = Dice("1d20").roll()
        if total == 20:
            LOG.DEBUG("!!ROLLED A CRITICAL HIT!!")
            return -1

        ability = self.strength

        # if the item is not a weapon, then there's no bonsuses to apply.
        if not isinstance(item,ItemWeapon):
            apply_modifiers     = False
            apply_proficiencies = False

        # Add our proficiency bonus and handle switching the ability based on the weapon's type of attack.
        did_proficiency_bonus = False
        did_finesse = False
        for property in item.properties:
            if apply_modifiers:
                # Finesse weapons allow you to use DEX or STR as modifier.  We'll take the larger of the two.
                if not did_finesse and property == Enum.ItemWeapon.FINESSE:
                    LOG.DEBUG("Using a 'finesse' weapon for ATK")
                    did_finesse = True
                    if self.strength.score > ability.score:
                        ability = self.strength
                    if self.dexterity.score > ability.score:
                        ability = self.dexterity
                # If we're using a ranged weapon and it wasn't a finesse weapon, then we have to use DEX for the roll.
                if not did_finesse and property == Enum.ItemWeapon.RANGE:
                    LOG.DEBUG("Using a 'ranged' weapon for ATK")
                    ability = self.dexterity
            if apply_proficiencies:
                # If we're proficient with this weapon, then add the proficiency bonus.
                if not did_proficiency_bonus and property in self.proficiencies:
                    did_proficiency_bonus = True
                    total += self.proficiency_bonus

        # Don't use our ability modifier if we're explictly not using it.
        if not apply_modifiers:
            ability = None

        # Finally let's add our modifier.
        if ability:
            if ability == self.strength:
                LOG.DEBUG("Using STR as ATK modifier: %s"%ability.__str__())
            elif ability == self.dexterity:
                LOG.DEBUG("Using DEX as ATK modifier: %s"%ability.__str__())
            total += ability.modifier

        # Print what we rolled.
        LOG.INFO("Rolled %d ATK with %s"%(total,item.name.upper()))
        return total

    def roll_damage(self,item=None,apply_modifiers=True):
        """
        Rolls our damage to see how much damage we deal with our main hand weapon.
        
        :param item: Item object to deal damage with.
        :type item: ItemWeapon
        :return: A dictionary that maps damage type to the amount of damage rolled.
        """
        LOG.DEBUG(Helper.line_str_small())
        if not item:
            item = Item.get("unarmed")
            LOG.WARN("No weapon provided, assuming we're unarmed!")

        LOG.DEBUG("Rolling for DMG with %s"%item.name.upper())

        damage_dict = dict()
        for dice in item.dice:
            # Do the roll.
            value = dice.roll()
            # Add modifier if we need to.
            if apply_modifiers:
                ability = self.strength
                did_finesse = False
                for property in item.properties:
                    # Finesse weapons allow you to use DEX or STR as modifier.  We'll take the larger of the two.
                    if not did_finesse and property == Enum.ItemWeapon.FINESSE:
                        LOG.DEBUG("Using a 'finesse' weapon for DMG")
                        did_finesse = True
                        if self.strength.score > ability.score:
                            ability = self.strength
                        if self.dexterity.score > ability.score:
                            ability = self.dexterity
                    # If we're using a ranged weapon and it wasn't a finesse weapon, then we have to use DEX for the roll.
                    if not did_finesse and property == Enum.ItemWeapon.RANGE:
                        LOG.DEBUG("Using a 'ranged' weapon for DMG")
                        ability = self.dexterity
                # Finally let's add our modifier.
                if ability == self.strength:
                    LOG.DEBUG("Using STR as DMG modifier: %s"%self.strength.__str__())
                elif ability == self.dexterity:
                    LOG.DEBUG("Using DEX as DMG modifier: %s"%self.strength.__str__())
                value += ability.modifier
            if value > 0:
                for type in dice.types:
                    if not type in damage_dict.keys():
                        damage_dict[type] = 0
                    LOG.INFO("Dealt %d %s DMG with %s"%(value,Enum.Damage.names[type],item.name.upper()))
                    damage_dict[type] += value
        return damage_dict




