from Helper import *
from Ability import *

class Condition(object):
    instances = list()
    def __init__(self,duration=0,inflicter=None):
        Condition.instances.append(self)
        self.duration = duration    # Int representing how many turns the condition last
        self.inflicter = inflicter  # Actor object that inflicted this condition

    def skill_check_advantage(self):
        return None
    def skill_check_disadvantage(self):
        return None
    def skill_check_fail(self):
        return None
    def skill_check_pass(self):
        return None

    def attack_advantage(self):
        return None
    def attack_disadvantage(self):
        return None
    def attack_fail(self):
        return None
    def attack_pass(self):
        return None

    def attacker_has_advantage(self):
        return None
    def attacker_has_disadvantage(self):
        return None
    def attacker_has_fail(self):
        return None
    def attacker_has_pass(self):
        return None

    def health_bonus(self):
        return None

    def armor_bonus(self):
        return None

    def saving_throw_bonus(self):
        return None

    def img_path(self):
        img_path = "/Users/bgrissom/Code/D20/assets/condition_"
        name = self.__class__.__name__
        prefix = "Condition"
        name = name[name.find(prefix)+len(prefix):]
        name = name.lower()

        if os.path.exists(img_path+name+".png"):
            return img_path+name+".png"
        else:
            return img_path+"default.png"


class ConditionHalfCover(Condition):
        def armor_bonus(self):
            return 2

        def saving_throw_bonus(self):
            return {AbilityDexterity(),2}
ConditionHalfCover()


class ConditionMostlyCover(Condition):
        def armor_bonus(self):
            return 5

        def saving_throw_bonus(self):
            return {AbilityDexterity(),5}
ConditionMostlyCover()


class ConditionBlinded(Condition):
    pass
ConditionBlinded()


class ConditionCharmed(Condition):
        pass
ConditionCharmed()


class ConditionFrightened(Condition):
        pass
ConditionFrightened()


class ConditionGrappled(Condition):
        pass
ConditionGrappled()


class ConditionIncapacitated(Condition):
        pass
ConditionIncapacitated()


class ConditionInvisible(Condition):
        pass
ConditionInvisible()


class ConditionParalyzed(Condition):
        pass
ConditionParalyzed()


class ConditionPetrified(Condition):
        pass
ConditionPetrified()


class ConditionPoisoned(Condition):
        pass
ConditionPoisoned()


class ConditionProne(Condition):
        pass
ConditionProne()


class ConditionProne(Condition):
        pass
ConditionProne()


class ConditionRestrained(Condition):
        pass
ConditionRestrained()


class ConditionStunned(Condition):
        pass
ConditionStunned()


class ConditionUnconscious(Condition):
        pass
ConditionUnconscious()


class ConditionExhausted(Condition):
        pass
ConditionExhausted()