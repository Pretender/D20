import random
from Helper import *

class Dice(object):
    def __init__(self,number=1,value=20):
        """
        Creates a dice object that simulates rolling a dice.

        :param number: Either a number indicating how many rolls to perform, or a string of the format '1d10'.
        :param value: If number was an int, then this value will be used to determine the number of sides on the die.
        """
        self.num_rolls = number
        self.value = value
        if isinstance(number,basestring):
            number = number.lower()
            [number,value] = number.split("d")
            self.num_rolls = int(number)
            self.value = int(value)

    def roll(self):
        """
        Rolls our die and returns the value that we rolled.

        :return: Value rolled.
        """
        value = 0
        for x in range(0,self.num_rolls):
            value += random.randint(1,self.value)
        LOG.DEBUG("Rolled a %dd%d for %d"%(self.num_rolls,self.value,value))
        return value

    def __str__(self):
        return "%dd%d"%(self.num_rolls,self.value)

class DiceDamage(Dice):
    def __init__(self,number=0,value=0):
        """
        Creates a damage dice.  This dice contains a list of damage types that cna be inflicted by this dice.

        :param number: Either a number indicating how many rolls to perform, or a string of the format '1d10'.
        :param value: If number was an int, then this value will be used to determine the number of sides on the die.
        """
        Dice.__init__(self,number,value)
        self.types = list() # The various types of damage this roll inflicts.

    def __str__(self):
        type_str = ""
        for type in self.types:
            type_str += Enum.Damage.names[type].lower() + "/"
        type_str = type_str.rstrip("/")
        return Dice.__str__(self) + " " + type_str

if __name__ == "__main__":
    d = Dice("2d10")
    print d.roll()