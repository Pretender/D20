import json
import os
from Actor import *
from Condition import *

class GameData(object):
    def __init__(self,dir):
        # Define our dictionaries that store our data in a pythonic manner.
        self.races       = dict()
        self.classes     = dict()
        self.monsters    = dict()
        self.spells      = dict()
        self.items       = dict()

        # Modify our dictionary string if need be.
        if not dir.endswith("/"):
            dir += "/"

        # If the directory doesn't exist, then we'll make it.
        if not os.path.exists(dir):
            os.makedirs(dir)

        # Otherwise attempt to load whatever data is already in the directory.
        else:
            # Bail if the path isn't a directory.
            if not os.path.isdir(dir):
                raise Exception("Game path is not a directory!")
            # Load our data files.
            json_files = list()
            json_files.append("races.json")
            json_files.append("classes.json")
            json_files.append("monsters.json")
            json_files.append("spells.json")
            json_files.append("items.json")
            for json_file in json_files:
                json_path = dir + json_file
                if not os.path.exists(json_path):
                    continue
                json_data = dict()
                try:
                    with open(json_path) as f:
                        json_data = json.load(f)
                except Exception,e:
                    raise e
                if "races" in json_file:
                    self.parseJsonRaces(json_data)
                if "classes" in json_file:
                    self.parseJsonClasses(json_data)
                elif "monsters" in json_file:
                    self.parseJsonMonsters(json_data)
                elif "spells" in json_file:
                    self.parseJsonSpells(json_data)
                elif "items" in json_file:
                    self.parseJsonItems(json_data)

    def parseJsonRaces(self,json_data):
        pass

    def parseJsonClasses(self,json_data):
        pass

    def parseJsonMonsters(self,json_data):
       pass

    def parseJsonItems(self,json_file):
        import re
        def parseItemData(item,data):
            item.name = Helper.dictEntry(data,"name",item.name)
            item.desc = Helper.dictEntry(data,"description",item.desc)
            value = Helper.dictEntry(data,"cost",None)
            if value:
                item.value = Money(value)
            item.weight = float(Helper.dictEntry(data,"weight",item.weight))
            item.consumable = Helper.dictEntry(data,"consumable",item.desc)

        # Load our weapons.
        json_data = Helper.dictEntry(json_file,"weapons",dict())
        for key,data in json_data.items():
            item = ItemWeapon(key)
            parseItemData(item,data)

            # Parse raw damage number.
            item.raw_damage = int(Helper.dictEntry(data,"damage_raw",item.raw_damage))

            # Parse weapon properties.
            json_props = Helper.dictEntry(data,"properties")
            if json_props:
                json_props.split(",")
                for json_prop in json_props.split(","):
                    json_prop = json_prop.strip()
                    json_prop = json_prop.replace("-","_")
                    json_prop = json_prop.upper()
                    prop = None
                    if isinstance(json_prop,basestring):
                        prop = Enum.ItemWeapon.values[json_prop.upper()]
                    if prop:
                        item.properties.append(prop)

            # Parse damage type and dice.
            dmg_str = Helper.dictEntry(data,"damage")
            if dmg_str:
                dmg_strs = dmg_str.split(" ")
                dice = None
                for dmg_str in dmg_strs:
                    dmg_str = dmg_str.upper()
                    a = re.compile("^([0-9]+d[0-9]+)*$",re.IGNORECASE)
                    if bool(a.match(dmg_str)):
                        dice = DiceDamage(dmg_str)
                    else:
                        dice.types.append(Enum.Damage.values[dmg_str])
                item.dice.append(dice)
        self.logWeapons()

    def parseJsonSpells(self,json_data):
        pass

    def logWeapons(self):
        headers = ["Name","Cost","Damage","Weight","Properties"]
        table = [headers]
        num_weapons = 0
        for key,item in Item.instances.items():
            if not isinstance(item,ItemWeapon):
                continue

            row = list()
            row.append(item.name)
            row.append(item.value)

            dice_str = ""
            for dice in item.dice:
                dice_str += dice.__str__() + ", "
            dice_str = dice_str.rstrip(", ")
            row.append(dice_str)

            row.append(item.weight)

            properties_str = ""
            for property in item.properties:
                properties_str += Enum.ItemWeapon.names[property].lower() + ", "
            properties_str = properties_str.rstrip(", ")
            row.append(properties_str)
            table.append(row)
            num_weapons += 1
        LOG.INFO(Helper.line_str_small())
        LOG.TABLE(table)
        LOG.INFO(Helper.line_str_small())
        LOG.INFO("Loaded %d weapons"%num_weapons)




        
        
    
    