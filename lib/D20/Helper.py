import logging
import StringIO
import sys, os


class LOG(object):
    instance = logging.getLogger("D20")

    @classmethod
    def setup(cls,log_path=None):
        if not log_path:
            log_path = '~/Library/Logs/D20.log'
        log_path = os.path.expanduser(log_path)
        if os.path.exists(log_path):
            os.remove(log_path)
        handler = logging.FileHandler(log_path)
        cls.instance.addHandler(handler)
        cls.instance.setLevel(logging.DEBUG)

    @classmethod
    def DEBUG(cls,msg):
        cls.instance.debug(msg)

    @classmethod
    def INFO(cls,msg):
        cls.instance.info(msg)

    @classmethod
    def WARN(cls,msg):
        cls.instance.warning(msg)

    @classmethod
    def ERROR(cls,msg):
        cls.instance.error(msg)

    @classmethod
    def TABLE(cls,table):
        def format_num(num):
            try:
                inum = int(num)
                return str(inum)

            except (ValueError, TypeError):
                return str(num)
        def get_max_width(table, index):
            return max([len(format_num(row[index])) for row in table])
        out = StringIO.StringIO()
        col_paddings = []
        for i in range(len(table[0])):
            col_paddings.append(get_max_width(table, i))

        for row in table:
            # left col
            print >> out, row[0].ljust(col_paddings[0] + 1),
            # rest of the cols
            for i in range(1, len(row)):
                col = format_num(row[i]).rjust(col_paddings[i] + 2)
                print >> out, col,
            print >> out
        LOG.INFO(out.getvalue())

class Helper(object):
    @classmethod
    def enum(*sequential, **named):
        """
        Generates an enumerated type.  This pairs an integer to a name that' easy to reference.  Included in the returned structure is a name dictionary that maps the enum to its
        string name.
        """
        enums = dict(zip(sequential, range(len(sequential))), **named)
        reverse = dict((value, key) for key, value in enums.iteritems())
        reverse2 = dict((key, value) for key, value in enums.iteritems())
        enums['names']  = reverse
        enums['values'] = reverse2
        return type('Enum', (), enums)

    @classmethod
    def dictEntry(cls,dict,key,default=None):
        """
        Returns the value for key in dict.  If key doesn't exist, then default is returned.

        :param dict: Dictionary to search for key in.
        :param key: Key to look for in dictionary.
        :param default: Value returned if key is not found.
        :return: Either the value in the dictionary or default.
        """
        value = default
        try:
            value = dict[key]
        except:
            pass
        return value

    @classmethod
    def compareStr(cls,str1,str2,case_sensitive=False):
        """
        Compares two strings to see if they're the same.

        :param str1: A string.
        :param str2: Other string to compare str1 to.
        :param case_sensitive: If set, will care about case, otherwise we don't.
        :return: Boolean indicating if strings match.
        """
        if case_sensitive:
            return str1 == str2
        else:
            return str1.lower() == str2.lower()

    @classmethod
    def line_str_small(cls):
        return "----------------------------------------"

class Enum(object):

    ItemArmor = Helper.enum('LIGHT','MEDIUM','HEAVY','SHIELD','OTHER')
    # Represents the various types of armor that a user can equip.

    ItemWeapon = Helper.enum('FINESSE','HEAVY','LIGHT','LOADING','RANGE','REACH','SPECIAL','THROWN','TWO_HANDED','VERSATILE','SIMPLE','MARTIAL','OTHER')
    # Represents the various items that a user can equip.

    Damage = Helper.enum('ACID','BLUDGEONING','COLD','FIRE','FORCE','LIGHTNING','NECROTIC','PIERCING','POISON','PSYCHIC','RADIANT','SLASHING','THUNDER')
    # Represents the various types of damage that can be dealt.

    def __init__(self):
        pass