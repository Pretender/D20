from Money import *
from Dice import *

class Item(object):
    instances = dict() # Static dictionary of items mapped by key (string) to Item object.

    def __init__(self,key):
        """
        Defines an item that can be used by actors in the world.  This data is typically loaded from a .json file and parsed into this object by GameData.  Since this object
        defines the data that makes up an item, there is one object per item in a game.  Actor objects using an object, simply reference the item.  Thus, this object shouldn't
        be modified by any of the classes after having been loaded from the data file.

        :param key: The key of this item in the .json file.  This is also how the item will be referenced throughout the code.
        """
        Item.instances[key] = self      # Add this item to our list of item objects so we can easily recall it.
        self.name           = key       # The name of this item.
        self.desc           = ""        # Flavor text about this item.
        self.value          = Money(0)  # How much this item is worth.
        self.weight         = 0.0       # How much this item weighs.
        self.consumable     = False     # If this item is a consumable, then the player loses it after it is used.

    @classmethod
    def get(cls,key):
        """
        Returns the Item object associated with the given key.  If we can't find the requested item, then an exception is thrown.

        :param key: The key of the item you want ot fetch.
        :return: Item object with the associated key.
        """
        try:
            return Item.instances[key]
        except:
            raise Exception("Could not find item with id: '%s'"%key)

class ItemWeapon(Item):
    def __init__(self,key):
        Item.__init__(self,key)
        self.raw_damage   = 0       # Raw damage this weapon applies.
        self.properties   = list()  # List of properties this weapon has.
        self.dice         = list()  # List of DiceDamage objects.

class ItemArmor(Item):
    def __init__(self,key):
        Item.__init__(self,key)
        self.raw_armor      = 0      # How much armor this class adds.
        self.disadvantages  = list() # List of skills that have disadvantage when using this armor.
        self.abilities      = list() # List of abilities that add on to upping this armor value.
        self.abilities_max  = list() # List of the maximum amount of bonus an ability can add.
        self.type           = Enum.ItemArmor.Other # What kind of armor this.



