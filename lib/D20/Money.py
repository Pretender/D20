class Money(object):
    def __init__(self,amount=None):
        """
        Represents an amount of money in different types of coin-value.  The types of coin-value are as follows:

        Copper Piece   (CP) = Lowest unit
        Silver Piece   (SP) = 10 CPs
        Electrum Piece (EP) = 5 SPs or 50 CPs
        Gold Piece     (EP) = 10 SPs or 100 CPs
        Platinum Piece (PP) = 10 GPs or 1000 CPs

        :param amount:  Either a string or int representing the amount this object represents.
        """
        self.pp = 0 # 1000
        self.gp = 0 # 100
        self.ep = 0 # 50
        self.sp = 0 # 10
        self.cp = 0

        # If no amount was provided then there's nothing to compute.
        if amount == None:
            return

        # If amount provided was a string, then parse it.
        if isinstance(amount,basestring):
            str_so_far = ""
            amount = amount.upper()
            for char in amount:
                str_so_far += char
                reset = False
                if str_so_far.endswith("CP"):
                    self.cp = int(str_so_far[:str_so_far.find("CP")].strip())
                    reset = True
                if str_so_far.endswith("SP"):
                    self.sp = int(str_so_far[:str_so_far.find("SP")].strip())
                    reset = True
                if str_so_far.endswith("GP"):
                    self.gp = int(str_so_far[:str_so_far.find("GP")].strip())
                    reset = True
                elif str_so_far.endswith("EP"):
                    self.ep = int(str_so_far[:str_so_far.find("EP")].strip())
                    reset = True
                elif str_so_far.endswith("PP"):
                    self.pp = int(str_so_far[:str_so_far.find("PP")].strip())
                    reset = True
                if reset:
                    str_so_far = ""

        # Otherwise, we assume a raw int value was provided and must figure out what it was.
        else:
            amount = int(amount)
            self.pp = amount / 1000
            extra = amount % 1000
            if extra:
                self.gp = extra / 100
                extra = extra % 100
                if extra:
                    self.ep = extra / 50
                    extra = extra % 50
                    if extra:
                        self.sp = extra / 10
            self.cp = amount % 10

    @property
    def value_list(self):
        """
        :return: List of ints representing the value for each coin-type.  The order is: [CP,SP,EP,GP,PP]
        """
        return [self.cp,self.sp,self.ep,self.gp,self.pp]

    @property
    def value_raw(self):
        """
        :return: Integer representing the raw value.
        """
        return self.cp+self.sp*10+self.ep*50+self.gp*100+self.pp*1000
    
    @property
    def value_str(self):
        """
        :return: String representing the value of this object in CP/PP notation.
        """
        value = ""
        if self.pp > 0:
            value += str(self.pp) + "PP "
        if self.gp > 0:
            value += str(self.gp) + "GP "
        if self.ep > 0:
            value += str(self.ep) + "EP "
        if self.sp > 0:
            value += str(self.sp) + "SP "
        if self.cp > 0:
            value += str(self.cp) + "CP "
        value = value.rstrip()
        return value

    def __str__(self):
        return self.value_str

    def __add__(self,other):
        total = self.value_raw + other.value_raw
        return Money(total)

    def __sub__(self, other):
        total = self.value_raw - other.value_raw
        return Money(total)

    def __mul__(self, other):
        total = self.value_raw * other.value_raw
        return Money(total)

    def __div__(self, other):
        total = 0
        try:
            total = self.value_raw / other.value_raw
        except:
            pass
        return Money(total)

if __name__ == "__main__":
    amount = 1565
    m = Money(amount)
    m = Money(m.value_str)
    print "Test Constructor:",amount == m.value_raw

    v1 = 1756
    v2 = 8967
    m1 = Money(v1)
    m2 = Money(v2)
    m3 = m1 + m2
    print "Test Adding:",m3.value_raw == (v1+v2)

    v1 = 8967
    v2 = 175
    m1 = Money(v1)
    m2 = Money(v2)
    m3 = m1 - m2
    print "Test Subtract:",m3.value_raw == (v1-v2)

    v1 = 8967
    v2 = 175
    m1 = Money(v1)
    m2 = Money(v2)
    m3 = m1 * m2
    print "Test Multiply:",m3.value_raw == (v1*v2)

    v1 = 8967
    v2 = 175
    m1 = Money(v1)
    m2 = Money(v2)
    m3 = m1 / m2
    print "Test Divide:",m3.value_raw == (v1/v2)


    v1 = 175
    v2 = 8967
    m1 = Money(v1)
    m2 = Money(v2)
    m3 = m1 / m2
    print "Test Divide 2:",m3.value_raw == (v1/v2)

    v1 = 0
    v2 = 8967
    m1 = Money(v1)
    m2 = Money(v2)
    m3 = m1 / m2
    print "Test Divide 3:",m3.value_raw == (v1/v2)
