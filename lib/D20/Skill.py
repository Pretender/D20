class Skill(object):
    def __init__(self,name="",desc="",ability=None,proficient=False):
        """
        Represents a specific aspect of an ability score.
        """
        self.name       = name        # String representing the name of this skill.
        self.desc       = desc        # String describing this skill.
        self.ability    = ability     # Ability object associated with this skill.
        self.proficient = proficient  # Boolean that indicates whether or not an actor is proficient with this skill.

    def __str__(self):
        value = self.ability.modifier
        if self.ability.modifier >= 0:
            return "+%d"%value
        else:
            return "%d"%value

class SkillAthletics(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="athletics",ability=abilities.strength)

class SkillAcrobatics(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="acrobatics",ability=abilities.dexterity)

class SkillSleightOfHand(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="sleight of hand",ability=abilities.dexterity)

class SkillStealth(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="stealth",ability=abilities.dexterity)

class SkillArcana(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="arcana", ability=abilities.intelligence)

class SkillHistory(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="history",ability=abilities.intelligence)

class SkillInvestigation(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="investigation",ability=abilities.intelligence)

class SkillNature(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="nature",ability=abilities.intelligence)

class SkillReligion(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="religion",ability=abilities.intelligence)

class SkillAnimalHandling(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="animal handling",ability=abilities.intelligence)

class SkillInsight(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="insight",ability=abilities.wisdom)

class SkillMedicine(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="medicine",ability=abilities.wisdom)

class SkillPerception(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="perception",ability=abilities.wisdom)

class SkillSurvival(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="survival",ability=abilities.wisdom)

class SkillDeception(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="deception",ability=abilities.charisma)

class SkillIntimidation(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="intimidation",ability=abilities.charisma)

class SkillPerformance(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="performance",ability=abilities.charisma)

class SkillPersuasion(Skill):
    def __init__(self,abilities):
        Skill.__init__(self,name="persuasion",ability=abilities.charisma)

class Skills(object):
    def __init__(self,abilities=None):
        self.athletics          = SkillAthletics(abilities)
        self.acrobatics         = SkillAcrobatics(abilities)
        self.sleight_of_hand    = SkillSleightOfHand(abilities)
        self.stealth            = SkillStealth(abilities)
        self.arcana             = SkillArcana(abilities)
        self.history            = SkillHistory(abilities)
        self.investigation      = SkillInvestigation(abilities)
        self.nature             = SkillNature(abilities)
        self.religion           = SkillReligion(abilities)
        self.animal_handling    = SkillAnimalHandling(abilities)
        self.insight            = SkillInsight(abilities)
        self.medicine           = SkillMedicine(abilities)
        self.perception         = SkillPerception(abilities)
        self.survival           = SkillSurvival(abilities)
        self.deception          = SkillDeception(abilities)
        self.intimidation       = SkillIntimidation(abilities)
        self.performance        = SkillPerformance(abilities)
        self.persuasion         = SkillPersuasion(abilities)


