from WindowWidget import *
from .. import D20

class ActorWidget(Widget):
    actor       = ObjectProperty(None)
    health      = StringProperty("0")
    health_max  = StringProperty("0")
    armor       = StringProperty("0")
    speed       = StringProperty("0")
    str         = StringProperty("0")
    dex         = StringProperty("0")
    con         = StringProperty("0")
    int         = StringProperty("0")
    wis         = StringProperty("0")
    cha         = StringProperty("0")

    def __init__(self, **kwargs):
        super(ActorWidget,self).__init__(**kwargs)

        self.actor = kwargs["actor"]
        """ :type: D20.Actor """

        self.name   = self.actor.name
        self.health = str(self.actor.health)
        self.health_max = str(self.actor.health_max)
        self.armor = str(self.actor.armor)
        self.speed = str(self.actor.speed/5)


