from kivy.logger import Logger
from kivy.uix.image import Image
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import NumericProperty, ObjectProperty, StringProperty, ListProperty
from .. import D20

class ConditionButton(Image):
    condition = ObjectProperty(None)
    pass

class ConditionsWidget(BoxLayout):
    actor = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ConditionsWidget,self).__init__(**kwargs)
        for condition in D20.Condition.instances:
            self.add_widget(ConditionButton(condition=condition))




