from kivy.uix.widget import Widget
from kivy.properties import NumericProperty,StringProperty

class DebugBox(Widget):
    red   = NumericProperty(0)
    green = NumericProperty(0)
    blue  = NumericProperty(0)
    label = StringProperty("")


