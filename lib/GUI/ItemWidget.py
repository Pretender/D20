from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.properties import ObjectProperty
from .. import D20

class ItemLabel(Label):
    pass

class ItemWidget(Widget):
    item       = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ItemWidget,self).__init__(**kwargs)
        self.item = kwargs["item"]
        """ :type: D20.Item """



