from kivy.logger import Logger
from kivy.app import App
from kivy.uix.label import Label
from DebugBox import *
from ActorWidgetView import *
from ItemWidgetView import *
from ConditionsWidget import *
from WindowWidget import *

class ShadowLabel(Label):
    decal = ListProperty([0, 0])
    tint = ListProperty([1, 1, 1, 1])

class ActorStatLabel(ShadowLabel):
    pass

class ActorAttrLabel(ShadowLabel):
    pass

class Main(Widget):
    pass

class D20App(App):
    def build(self):
        main = Main()
        players = list()
        player = D20.Actor("Eridon",str=8,dex=18,con=12,int=8,wis=10,cha=18)
        player.armor        = 15
        player.health       = 9
        player.health_max   = 9
        player.speed        = 30
        player.dexterity.saving_throw = True
        player.intelligence.saving_throw = True
        players.append(player)

        player = D20.Actor("Ricardo",str=9,dex=9,con=16,int=15,wis=11,cha=16)
        player.armor        = 12
        player.health       = 9
        player.health_max   = 9
        player.speed        = 30
        player.constitution.saving_throw = True
        player.charisma.saving_throw = True
        players.append(player)

        player = D20.Actor("Aioua",str=12,dex=18,con=9,int=9,wis=16,cha=9)
        player.armor        = 15
        player.health       = 10
        player.health_max   = 10
        player.speed        = 30
        player.strength.saving_throw = True
        player.dexterity.saving_throw = True
        players.append(player)

        player = D20.Actor("Vandal",str=18,dex=14,con=13,int=12,wis=9,cha=9)
        player.armor        = 16
        player.health       = 11
        player.health_max   = 11
        player.speed        = 30
        player.strength.saving_throw = True
        player.constitution.saving_throw = True
        players.append(player)

        for player in players:
            actor = ActorWidget(actor=player)
            main.players.layout.add_widget(actor)

        monsters = list()
        monster = D20.Actor("Goblin",str=8,dex=14,con=10,int=10,wis=8,cha=8)
        monster.armor        = 15
        monster.health       = 7
        monster.health_max   = 7
        monster.speed        = 30
        monsters.append(monster)

        monster = D20.Actor("Goblin",str=8,dex=14,con=10,int=10,wis=8,cha=8)
        monster.armor        = 15
        monster.health       = 7
        monster.health_max   = 7
        monster.speed        = 30
        monsters.append(monster)

        monster = D20.Actor("Goblin",str=8,dex=14,con=10,int=10,wis=8,cha=8)
        monster.armor        = 15
        monster.health       = 7
        monster.health_max   = 7
        monster.speed        = 30
        monsters.append(monster)

        monster = D20.Actor("Goblin",str=8,dex=14,con=10,int=10,wis=8,cha=8)
        monster.armor        = 15
        monster.health       = 7
        monster.health_max   = 7
        monster.speed        = 30
        monsters.append(monster)

        monster = D20.Actor("Goblin",str=8,dex=14,con=10,int=10,wis=8,cha=8)
        monster.armor        = 15
        monster.health       = 7
        monster.health_max   = 7
        monster.speed        = 30
        monsters.append(monster)

        for monster in monsters:
            monster.img_window   = "/Users/bgrissom/Code/D20/assets/bg_red.png"
            actor = ActorWidget(actor=monster)
            main.baddies.layout.add_widget(actor)

        for key,item in D20.Item.instances.items():
            main.items.layout.add_widget(ItemWidget(item=item))


        return main