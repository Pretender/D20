from lib import GUI, D20
import os

D20.LOG.setup("~/Downloads/d20.log")
D20.GameData(os.path.dirname(os.path.realpath(__file__))+"/sample")

from kivy.config import Config
Config.set('graphics', 'width', '1440')
Config.set('graphics', 'height', '920')
#Config.set('graphics', 'resizable', '0')

app = GUI.D20App()
app.run()

# import os
#
# from lib import D20
#
# D20.LOG.setup()
#
# dir = os.path.dirname(os.path.realpath(__file__)) + "/sample"
#
# data = D20.GameData(dir)
#
# player = D20.Actor()
# player.name = "Eridon"
# player.strength.score       = 8
# player.dexterity.score      = 18
# player.constitution.score   = 12
# player.intelligence.score   = 8
# player.wisdom.score         = 10
# player.charisma.score       = 16
#
# goblin = D20.Actor()
# goblin.name = "Goblin"
# goblin.strength.score       = 8
# goblin.dexterity.score      = 14
# goblin.constitution.score   = 10
# goblin.intelligence.score   = 10
# goblin.wisdom.score         = 8
# goblin.charisma.score       = 8
#
# dagger = D20.Item.get("dagger")
#
# atk = player.roll_attack(dagger)
# if atk == -1:
#     player.roll_damage(dagger)
#     player.roll_damage(dagger)
# elif atk > 8:
#     player.roll_damage(dagger)
# else:
#     D20.LOG.INFO("The attack missed!")